#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):

        self.tags = []
        self.dicc = {'root-layout': ['width', 'height', 'background-color'],
                     'region': ['id', 'top', 'bottom', 'left', 'right'],
                     'img': ['src', 'region', 'begin', 'dur'],
                     'audio': ['src', 'begin', 'dur'],
                     'textstream': ['src', 'region']}

    def startElement(self, nombre, valor):

        if nombre in self.dicc:

            tmpdic = {}
            for atributo in self.dicc[nombre]:
                tmpdic[atributo] = valor.get(atributo, "")
            self.tags.append([nombre, tmpdic])

    def get_tags(self):
        return self.tags


if __name__ == "__main__":
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    misDatos = cHandler.get_tags()
    print(misDatos)
